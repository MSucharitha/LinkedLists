/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   listType.hpp
 * Author: Ramanand
 *
 * Created on September 6, 2017, 2:51 PM
 */
#include <iostream>
#include <assert.h>
using namespace std;

#ifndef LISTTYPE_HPP
#define LISTTYPE_HPP

#include "listiterators.hpp"

template <class Type>
class linkedListType
{
public:
const linkedListType<Type>& operator=(const linkedListType<Type>&);
//Overload the assignment operator.
void initializeList();
//Initialize the list to an empty state.
//Postcondition: first = NULL, last = NULL, count = 0;
bool isEmptyList() const;
//Function to determine whether the list is empty.
//Postcondition: Returns true if the list is empty, otherwise
// it returns false.
void print() const;
//Function to output the data contained in each node.
//Postcondition: none
int length() const;
//Function to return the number of nodes in the list.
//Postcondition: The value of count is returned.
void destroyList();
//Function to delete all the nodes from the list.
//Postcondition: first = NULL, last = NULL, count = 0;
Type front() const;
//Function to return the first element of the list.
//Precondition: The list must exist and must not be empty.
//Postcondition: If the list is empty, the program terminates;
// otherwise, the first element of the list is returned.
Type back() const;
//Function to return the last element of the list.
//Precondition: The list must exist and must not be empty.
//Postcondition: If the list is empty, the program
// terminates; otherwise, the last
// element of the list is returned.
virtual bool search(const Type& searchItem) const = 0;
//Function to determine whether searchItem is in the list.
//Postcondition: Returns true if searchItem is in the list,
// otherwise the value false is returned.
virtual void insertFirst(const Type& newItem) = 0;
//Function to insert newItem at the beginning of the list.
//Postcondition: first points to the new list, newItem is
// inserted at the beginning of the list, last points to
// the last node in the list, and count is incremented by
// 1.
virtual void insertLast(const Type& newItem) = 0;
//Function to insert newItem at the end of the list.
//Postcondition: first points to the new list, newItem is
// inserted at the end of the list, last points to the
// last node in the list, and count is incremented by 1.
virtual void deleteNode(const Type& deleteItem) = 0;
//Function to delete deleteItem from the list.
//Postcondition: If found, the node containing deleteItem is
// deleted from the list. first points to the first node,
// last points to the last node of the updated list, and
// count is decremented by 1.
linkedListIterator<Type> begin();
//Function to return an iterator at the beginning of the
//linked list.
//Postcondition: Returns an iterator such that current is set
// to first.
linkedListIterator<Type> end();
//Function to return an iterator one element past the
//last element of the linked list.
//Postcondition: Returns an iterator such that current is set
// to NULL.
linkedListType();
//default constructor
//Initializes the list to an empty state.
//Postcondition: first = NULL, last = NULL, count = 0;
linkedListType(const linkedListType<Type>& otherList);
//copy constructor
~linkedListType();
//destructor
//Deletes all the nodes from the list.
//Postcondition: The list object is destroyed.
protected:
int count; //variable to store the number of list elements
//
nodeType<Type> *first; //pointer to the first node of the list
nodeType<Type> *last; //pointer to the last node of the list
private:
void copyList(const linkedListType<Type>& otherList);
//Function to make a copy of otherList.
//Postcondition: A copy of otherList is created and assigned
// to this list.
};
template <class Type>
linkedListType<Type>::linkedListType() //default constructor
{
first = NULL;
last = NULL;
count = 0;
}
template<class Type>
void linkedListType<Type>::initializeList() 
{
    destroyList();
}
template<class Type>
bool linkedListType<Type>::isEmptyList() const
{
    return (first==NULL);
}
template<class Type>
void linkedListType<Type>::print() const
{
    nodeType<Type>* temp;
    temp=first;
    while(temp !=NULL)
    {
        cout<< temp->info << " " ;
        temp=temp->link;
    }
}
template<class Type>
int linkedListType<Type>::length() const
{
    return count;
}
template<class Type>
void linkedListType<Type>::destroyList()
{
    nodeType<Type>* temp;
    while(first !=NULL)
    {
        temp=first;
        first=first->link;
        delete temp;
    }
    last=NULL;
    count=0;
}
template<class Type>
Type linkedListType<Type>::front() const
{
    assert(first!=NULL);
    return first->info;
}
template<class Type>
Type linkedListType<Type>::back() const
{
   assert(last!=NULL);
    return last->info;
}
template <class Type>
linkedListIterator<Type> linkedListType<Type>::begin()
{
linkedListIterator<Type> temp(first);
return temp;
} 
template <class Type>
linkedListIterator<Type> linkedListType<Type>::end()
{
linkedListIterator<Type> temp(NULL);
return temp;
}
template <class Type>
linkedListType<Type>::~linkedListType() //destructor
{
destroyList();
}
template <class Type>
linkedListType<Type>::linkedListType(const linkedListType<Type>& otherList)
{
first = NULL;
copyList(otherList);
}//end copy constructor
template<class Type>
void linkedListType<Type>::copyList(const linkedListType<Type>& otherList)
{
    if(first != NULL)
    {
        destroyList();
    }if(otherList.first == NULL)
    {
        first=NULL;
        last=NULL;
        count=0;
    }else
    {
        count=otherList.count;
    nodeType<Type>* newNode;
    nodeType<Type>* current;
    current=otherList.first;
    first=new nodeType<Type>;
    first->info=current->info;
    first->link=NULL;
    last=first;
    current=current->link;
    while(current != NULL)
    {
        newNode=new nodeType<Type>;
        newNode->info=current->info;
        newNode->link=NULL;
        last->link=newNode;
        last=newNode;
        current=current->link;
    } //while
    } //else
}
template<class Type>
const linkedListType<Type>& linkedListType<Type>::operator=(const linkedListType<Type>& other)
{
    if(this != &other)
    {
        copyList(other);
    }
    return *this;
}
#endif /* LISTTYPE_HPP */

