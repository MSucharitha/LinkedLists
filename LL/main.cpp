/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: Ramanand
 *
 * Created on September 6, 2017, 11:38 AM
 */

#include <cstdlib>
//#include "listiterators.hpp"

#include <iostream> //Line 1
#include "unorderedlinkedlist.hpp" //Line 2
using namespace std;
int main() //Line 4
{
unorderedLinkedList<int> list1, list2; //Line 5
int num; //Line 6
cout << "Line 7: Enter numbers ending "<< "with -999." << endl; //Line 7
cin >> num; //Line 8
while (num != -999) //Line 9
{ //Line 10
list1.insertFirst(num); //Line 11
cin >> num; //Line 12
} //Line 13
cout << endl; //Line 14
cout << "Line 15: list1: "; //Line 15
list1.print(); //Line 16
cout << endl; //Line 17
list2 = list1; //test the assignment operator Line 18
cout << "Line 19: list2: "; //Line 19
list2.print(); //Line 20
cout << endl; //Line 21
cout << "Line 22: Enter the number to be "<< "deleted: "; //Line 22
cin >> num; //Line 23
cout << endl; //Line 24
list2.deleteNode(num); //Line 25
cout << "Line 26: After deleting "<< num << ", list2: " << endl; //Line 26
list2.print(); //Line 27
cout << endl; //Line 28
return 0; //Line 29
} //Line 30