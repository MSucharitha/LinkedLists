/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   unorderedlinkedlist.hpp
 * Author: Ramanand
 *
 * Created on September 6, 2017, 9:50 PM
 */

#ifndef UNORDEREDLINKEDLIST_HPP
#define UNORDEREDLINKEDLIST_HPP
#include "listType.hpp"

template<class Type>
class unorderedLinkedList: public linkedListType<Type>
{
public:
bool search(const Type& searchItem) const;
//Function to determine whether searchItem is in the list.
//Postcondition: Returns true if searchItem is in the list,
// otherwise the value false is returned.
void insertFirst(const Type& newItem);
//Function to insert newItem at the beginning of the list.
//Postcondition: first points to the new list, newItem is
// inserted at the beginning of the list, last points to
// the last node, and count is incremented by 1.
//
void insertLast(const Type& newItem);
//Function to insert newItem at the end of the list.
//Postcondition: first points to the new list, newItem is
// inserted at the end of the list, last points to the
// last node, and count is incremented by 1.
void deleteNode(const Type& deleteItem);
//Function to delete deleteItem from the list.
//Postcondition: If found, the node containing deleteItem

// is deleted from the list. first points to the first
// node, last points to the last node of the updated
// list, and count is decremented by 1.
};
template<class Type>
bool unorderedLinkedList<Type>::search(const Type& searchItem) const
{
    nodeType<Type> *temp;
    bool found=false;
    temp = first;
    while(temp!=NULL)
    {
       if(searchItem==temp->info)
       {
           found=true;
           break;
       }
       temp=temp->link;
    }
    return found;
}
template<class Type>
void unorderedLinkedList<Type>::insertFirst(const Type& newItem)
{
    nodeType<Type> *newNode;
    newNode=new nodeType<Type>;
    newNode->info = newItem;
    newNode->link=first;
    first=newNode;
    count++;
    if(last==NULL)
    {
        last=newNode;
    }
}
template<class Type>
void unorderedLinkedList<Type>::insertLast(const Type& newItem)
{
    nodeType<Type> *newNode;
    newNode=new nodeType<Type>;
    nodeType<Type> *temp;
    temp=first;
    newNode->info=newItem;
    newNode->link=NULL;
    if(first=NULL)
    {
        first=newNode;
        last=newNode;
        count++;
    }else{
    while(temp!=NULL)
    {
        temp=temp->link; 
    }
    temp->link=newNode;
    last=newNode;
    count++;
    }
}
template<class Type>
void unorderedLinkedList<Type>::deleteNode(const Type& newItem)
{
    nodeType<Type> *temp;
    nodeType<Type> *prev;
    temp=first;
    prev=first;
    bool found=false;
    if(first==NULL)
    {
        cout << "Empty list" <<endl;
    }
    else
    {
        if(first->info==newItem)
        {
            cout << "in if" <<endl;
            temp=first;
            first=first->link;
            count--;
            if(first==NULL)
            {
                last=NULL;
            }
            delete temp;
        }
        else
       {
            cout <<"-----------------------in else---------------------------------" <<endl;
            prev=first;
            temp=first->link;
        while((temp!=NULL) && (!found))
        {
            
            if(temp->info!=newItem)
            {
                prev=temp;
                temp=temp->link;
            }else
            {
                found=true;
            }
            
            
        }
      }
        if(found)
        {
            cout << "found"<<endl;
            prev->link=temp->link;
            //if(last==temp) //temp->link==NULL ?
            if(temp->link==NULL)
            {
                last=prev;
            } //last node in the list
            count--;
            delete temp;
        }else
        {
            cout<<"Item not found" <<endl;
        }
    }
}
#endif /* UNORDEREDLINKEDLIST_HPP */

