/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   sort.hpp
 * Author: Ramanand
 *
 * Created on October 16, 2017, 9:59 AM
 */
#include <vector>

#include <iostream>
#ifndef SORT_HPP
#define SORT_HPP
class Sort
{
public:
    vector<int> unSortedVector;
    vector<int> mergedArray;
   Sort(int n);
   int get_n();
   void set_n(int elements);
   void printArray(vector<int> arry);
   void selectionSort();
   void insertionSort();
   void quickSort(int left,int right);
   void mergeSort(int l,int r);
private:
    int findPivot(int left,int right);
    int partitionArray(int pivot,int low,int high);
    void swap1(int i,int j);
    void mergeArrays(int one,int two);
    int numElements;
    
};
void Sort::set_n(int ele)
{
    numElements=ele;
}
Sort::Sort(int num)
{
    set_n(num);
    
    int el;
   
    cout << "Enter elements" << endl;
    for(int i=0;i<numElements;i++)
    {
        cin >> el;
        unSortedVector.push_back(el);
        //mergedArray.push_back(0);
    }
    
}
void Sort::swap1(int i,int j)
{
    int temp;
    temp=unSortedVector[i];
    unSortedVector[i]=unSortedVector[j];
    unSortedVector[j]=temp;
    
    
}
void swap2(int *xp, int *yp)
{
    int temp = *xp;
    *xp = *yp;
    *yp = temp;
}
void Sort::printArray(vector<int> array)
{
    int arrLength=array.size();
    for(int i=0;i<arrLength;i++)
    {
        cout << array[i] << " " ;
    }
    cout << endl;
}
void Sort::selectionSort()
{
    int len=unSortedVector.size();
    int small;
    int small_index;
    for(int pass=0;pass<len-1;pass++)
    {
        small=unSortedVector[pass];
        small_index=pass;
        for(int j=pass+1;j<len;j++)
        {
            if(unSortedVector[j] < small)
            {
                small=unSortedVector[j];
                small_index=j;
            
            }
        }
       if(small_index != pass)
        {
            //swap2(&unSortedVector[small_index],&unSortedVector[pass]);
            swap1(pass,small_index);
        }
    
    }
   
}
void Sort::insertionSort()
{
    int currentElem;
    int len=unSortedVector.size();
    for(int i=1;i<len;i++)
    {
        currentElem=i;
        int temp=unSortedVector[currentElem];
        if(unSortedVector[currentElem] < unSortedVector[currentElem-1])
        {
            do{
          //move data
            unSortedVector[currentElem]=unSortedVector[currentElem-1];
            currentElem--;
            }while(currentElem > 0  && (unSortedVector[currentElem-1] > temp));
            unSortedVector[currentElem]=temp;
        }
    }

}

void Sort::quickSort(int lt,int rt)
{
    int pivot;
    int pivotIndex;
    if(lt >= rt)
    {
    return;
    }
    
        pivot=findPivot(lt,rt);
        pivotIndex=partitionArray(pivot,lt,rt);
        quickSort(lt,pivotIndex-1);
        quickSort(pivotIndex,rt);
    
}
int Sort::findPivot(int left, int right)
{
    int temp=(left+right)/2;
    swap1(right,temp);
    return unSortedVector[right];
}
int Sort::partitionArray(int pivot, int low, int high)
{
    while(low <= high)
    {
        while(unSortedVector[low] < pivot)
        {
            low++;
        }
         while(unSortedVector[high] > pivot)
        {
            high--;
        }
        if(low<=high)
        {
            swap1(low,high);
            low++;
            high--;
        }
    }
    return low;
}
void Sort::mergeSort(int leftEnd,int rightEnd)
{
    if(leftEnd >= rightEnd)
    {
        return;
    }
    int mid=(leftEnd+rightEnd)/2;
    mergeSort(leftEnd,mid);
    mergeSort(mid+1,rightEnd-1);
    mergeArrays(leftEnd,rightEnd);
    
}
void Sort::mergeArrays(int l,int r)
{
    int index=l;
    int left=(l+r)/2;
    int right=left+1;
    while((l <= left) && (right <= r ) )
    {
        cout << "in the while loop" << "l"<< l << "right" <<r << endl;
        if(unSortedVector[l] <= unSortedVector[right])
        {
            mergedArray.at(index)=unSortedVector[l];
            cout<<mergedArray.at(index) << "merged array elements" <<endl;
            l++;
            
        }
        else
        {
         mergedArray.at(index)=unSortedVector[right];
          right++;
        }
       index++; 
    }
    if(l != left) //left array is big
    {
        while(l <= left)
        {
            mergedArray.at(index)=unSortedVector[l];
            l++;
            index++;
            
        }
    }
    if(right != r)
    {
        while(right <= r)
        {
            mergedArray.at(index)=unSortedVector[right];
            right++;
            index++;
        }
    }
    
}
#endif /* SORT_HPP */

